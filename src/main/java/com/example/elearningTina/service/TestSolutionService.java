package com.example.elearningTina.service;

import com.example.elearningTina.model.Test;
import com.example.elearningTina.model.TestSolution;

import java.util.List;

public interface TestSolutionService {

    TestSolution findOne(Long id);


    List<TestSolution> findAll();


    TestSolution save(TestSolution test);


    List<TestSolution> save(List<TestSolution> test);


    TestSolution delete(Long id);
}
