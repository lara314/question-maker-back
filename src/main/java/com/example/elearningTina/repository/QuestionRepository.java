package com.example.elearningTina.repository;

import com.example.elearningTina.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
//    ILI OVOOOOO
//    extends CrudRepository<MovieCharacter, Long>
}
