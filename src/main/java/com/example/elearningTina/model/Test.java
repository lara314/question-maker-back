package com.example.elearningTina.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Test {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)  //AUTO
    @Column(name = "id")
    private Long id;

    @Column(length = 200, unique = true)
    private String title;

    @ManyToMany(cascade = CascadeType.MERGE) //ALL
    @JoinTable(name = "test_question",
            joinColumns = { @JoinColumn(name = "test_id") },
            inverseJoinColumns = { @JoinColumn(name = "question_id") })
    private List<Question> questions = new ArrayList<Question>();

    public Test(){}

    public Test(String title) {
        this.title = title;
    }

    public Test(Long id, String title, List<Question> questions) {
        this.id = id;
        this.title = title;
        this.questions = questions;
    }

    public Test(String title, List<Question> questions) {
        this.title = title;
        this.questions = questions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public void addQuestion(Question question){
        this.questions.add(question);

//        if(answer.getQuestion()!=this){
//            answer.setQuestion(this);
//        }
    }
}
