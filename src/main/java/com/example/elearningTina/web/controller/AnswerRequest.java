package com.example.elearningTina.web.controller;

public class AnswerRequest {

    private long id;

    private String answer;

    private long questionId;

    private boolean correct;

    public AnswerRequest(long id, String answer, long questionId, boolean correct) {
        this.id = id;
        this.answer = answer;
        this.questionId = questionId;
        this.correct = correct;
    }

    public long getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    public long getQuestionId() {
        return questionId;
    }

    public boolean isCorrect() {
        return correct;
    }
}
