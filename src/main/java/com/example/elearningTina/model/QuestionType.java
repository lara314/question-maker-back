package com.example.elearningTina.model;

public enum QuestionType {

    RADIO_BUTTON, SELECT_BOX;
}
