package com.example.elearningTina.web.controller;

import com.example.elearningTina.model.Answer;
import com.example.elearningTina.model.Question;
import com.example.elearningTina.service.AnswerService;
import com.example.elearningTina.service.QuestionService;
import com.example.elearningTina.web.dto.AnswerDTO;
import com.example.elearningTina.web.dto.ConverterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/api/questions/{questionId}/answers")
//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
public class ApiAnswerController {

    @Autowired
    private AnswerService answerService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private ConverterDTO converter;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<AnswerDTO>> get(@PathVariable Long questionId) {

        List<AnswerDTO> answers = AnswerDTO.from(questionService.getById(questionId).getAnswers());

        return new ResponseEntity<>(answers, HttpStatus.OK);
    }

//    NECEMO DOBAVLJATI SVE
//    @RequestMapping(method= RequestMethod.GET)
//    ResponseEntity<List<AnswerDTO>> getAnswers(){
//
//        List<Answer> answers = answerService.findAll();
//
//        return new ResponseEntity<>(AnswerDTO.from(answers), HttpStatus.OK);
//    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    ResponseEntity<AnswerDTO> getAnswer(@PathVariable Long id,
                                        @PathVariable Long questionId){

        Answer answer = answerService.findOne(id);

        if (answer == null){
            throw new  RuntimeException("There is no Answer with the given id");
        }

        return new ResponseEntity<>(AnswerDTO.from(answer), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<AnswerDTO> add(
            @RequestBody @Validated AnswerDTO newAnswer,
            @PathVariable Long questionId) {

        Answer answer = converter.toEntityAnwserWithoutId(newAnswer);
        Optional<Question> question = questionService.findOne(questionId);
        if (question.isEmpty()){
            throw new  RuntimeException("There is no Question with the given id");
        }

        answer = answerService.save(answer);  // DA LI MOZE I BEZ OVOGA?
        question.get().addAnswer(answer);
        questionService.save(question.get());

        return new ResponseEntity<>(converter.toDto(answer), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<AnswerDTO> edit(@PathVariable Long id,
                                           @RequestBody @Validated AnswerDTO editedAnswer,
                                           @PathVariable Long questionId) {

        if (id == null || !id.equals(editedAnswer.getId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Answer oldAnswer = answerService.findOne(id);
        if (oldAnswer == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Answer persisted = answerService.save(converter.toEntityAnwser(editedAnswer));

        return new ResponseEntity<>(converter.toDto(persisted), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<List<AnswerDTO>> editAllAnswersOfQuestion( // @PathVariable Long id,
                                          @RequestBody @Validated List<AnswerDTO> editedAnswersList,
                                          @PathVariable Long questionId) {

//        if (id == null || !id.equals(editedAnswersList.get(0).getId())) { //proveri id od prvog, mora biti bar jedan odgovor
//            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//        }
//
//        Answer oldAnswer = answerService.findOne(id);
//        if (oldAnswer == null) {
//            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//        }
        Optional<Question> question = questionService.findOne(questionId);
        if (question.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<Answer> finalAnswers = answerService.save(converter.toListOfAnswerEntities(editedAnswersList));
        question.get().setAnswers(finalAnswers);
//        Answer persisted = answerService.save(converter.);

        return new ResponseEntity<>(converter.toListOfDto(finalAnswers), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<AnswerDTO> delete(@PathVariable Long id,
                                             @PathVariable Long questionId) {

        Optional<Question> question = questionService.findOne(questionId);
        Answer answer = answerService.findOne(id);
        question.get().removeAnswer(answer);

        answerService.delete(id);
        questionService.save(question.get());

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

//    @RequestMapping(method=RequestMethod.POST,
//            consumes="application/json")
//    public ResponseEntity<AnswerDTO> createAnswer(
//            @RequestBody AnswerDTO newAnswer){
//
//        Question question = questionService.findOne(newAnswer.getQuestionId()).orElseThrow(RuntimeException::new); //Promeni Exception!!!
//
//        Answer answer = converter.toEntityAnwserWithoutId(newAnswer);
//
//        answer = answerService.save(answer);
////                Answer answer = answerService.save(converter.toEntityAnwserWithoutId(newAnswer));
//
//        return new ResponseEntity<>(
//                AnswerDTO.from(answer),
//                HttpStatus.CREATED);
//    }


//    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//    ResponseEntity<AnswerDTO> deleteAnswer(@PathVariable Long id) {
//
//        Answer answer = answerService.findOne(id);
//
//        if (answer == null){
//            throw new RuntimeException("You cannot delete a non-existing Answer");
//        }
//        answerService.delete(id);
//
//        return new ResponseEntity<>(AnswerDTO.from(answer), HttpStatus.OK);
//    }



}
