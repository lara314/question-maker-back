package com.example.elearningTina.service;

import com.example.elearningTina.model.Question;
import com.example.elearningTina.web.dto.QuestionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface QuestionService {

    Optional<Question> findOne(Long id);

    Question getById(Long id);

    List<Question> findAll();

    Page<Question> findAll(Pageable pageable);

    Question save(Question user);

    Question delete(Long id);

    Question edit(QuestionDTO questionDto);
}
