package com.example.elearningTina.web.controller;

import com.example.elearningTina.model.Answer;
import com.example.elearningTina.model.Question;
import com.example.elearningTina.model.Test;
import com.example.elearningTina.service.TestService;
import com.example.elearningTina.web.dto.AnswerDTO;
import com.example.elearningTina.web.dto.ConverterDTO;
import com.example.elearningTina.web.dto.QuestionDTO;
import com.example.elearningTina.web.dto.TestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/tests")
@CrossOrigin(origins = "*")
public class ApiTestController {

    @Autowired
    private TestService testService;

    @Autowired
    ConverterDTO converterDTO;

    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<List<TestDTO>> getTests() {

        //List<QuestionDTO> questions = questionService.findAll().stream().map(QuestionDTO::from).collect(Collectors.toList());
        List<TestDTO> retTests = testService.findAll().stream().map(test -> TestDTO.from(test)).collect(Collectors.toList());

        return new ResponseEntity<>(retTests, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST,
            consumes = "application/json")
    public ResponseEntity<TestDTO> createTest(
            @RequestBody TestDTO newTestDTO) {  // @Validated ce trebati!!

        List<Question> questions = converterDTO.toListOfQuestionsForTest(newTestDTO.getQuestions());

      //  Test test = converterDTO.toEntityTest(newTestDTO);
        Test test = new Test();
        test.setTitle(newTestDTO.getTitle());

        test = testService.save(test);

        test.setQuestions(questions); //mozda jedan po 1

        test = testService.save(test);

//        List<AnswerDTO> answerDTOS = newQestionDTO.getAnswers();
//        // AnswersDTO stizu sa fronta i nemaju questionId!!!!!!!!!!1
//
//        Question question = converterDTO.toEntityQuestionWithoutId(newQestionDTO);
//
//        question = questionService.save(question);  //ovde dobijam questionId!!!
//
//        for (AnswerDTO answerDto: answerDTOS) {
//            answerDto.setQuestionId(question.getId());
//        }
//
//        List<Answer> answers = converterDTO.toListOfAnswerEntitiesWithId(answerDTOS);
////        List<Answer> answers = converterDTO.toListOfAnswerEntitiesWithId(answerDTOS);
//
//        answers = answerService.save(answers); // tu ili ispod
//        //question.setAnswers(answers);   // probaj u petlji sa addAnswer
//
//        for (Answer a: answers) {
//            question.addAnswer(a);
//        }
//
//        question = questionService.save(question);
//        //ovde dodaj odgovore na pitanje koje ima id jer je bilo u bazi
////        answer = answerService.save(answer);
////        question.get().addAnswer(answer);
////        questionService.save(question.get());

        return new ResponseEntity<>(
                TestDTO.from(test),
                HttpStatus.CREATED);
    }
}
